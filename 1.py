import cv2 as cv
import numpy as np
from matplotlib import pyplot as plt


def findIndex(arr, averge):
    return np.abs(arr-averge).argmin()


def getMeanIndex(arr):
    averge = np.mean(arr)
    index = findIndex(arr, averge)
    return [index, averge]


images = [['image/Fig0940(a)(rice_image_with_intensity_gradient).tif',11],
          ['image/Fig0938(a)(cygnusloop_Xray_original).tif',9],
          ['image/Fig0943(a)(dark_blobs_on_light_background).tif',3] ]

for i in range(len(images)):
    img = cv.imread(images[i][0])
    img = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    blimg = cv.medianBlur(img, images[i][1])
    #blimg = img

    hist = cv.calcHist([blimg], [0], None, [256], [0, 256])
    histU = getMeanIndex(hist)
    T = histU[0]
    while True:
        G1Data = getMeanIndex(hist[0:T])
        G2Data = getMeanIndex(hist[T:256])
        u = (G1Data[1] + G2Data[1])/2
        T1 = findIndex(hist, u)

        if abs(T1-T) < 10:
            T = T1
            break
        T = T1
    print(T)
    ret, th = cv.threshold(img, T, 255, cv.THRESH_BINARY)

    plt.figure(i)
    plt.subplot(1, 2, 1)
    plt.imshow(img, cmap='gray')
    plt.subplot(1, 2, 2)
    plt.imshow(th, cmap='gray')
    plt.show()